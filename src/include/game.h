#ifndef GAME_H
#define GAME_H

#include <pcg_variants.h>
#include "./card.h"

void gameLoop(pcg32_random_t *randSeed);

#endif